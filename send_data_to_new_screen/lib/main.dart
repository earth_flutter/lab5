import 'package:flutter/material.dart';

void main() {
  runApp(MaterialApp(
    title: 'Passing data',
    home: TodoScreen(
      todo: List.generate(20, (i) => Todo(
        'Todo $i',
         'A description of what needs to be done for Todo $i'),
         ),
         ),
  ));
}

class Todo {
  final String title;
  final String description;

  const Todo(this.title, this.description);
}

class TodoScreen extends StatelessWidget {
  const TodoScreen({Key? key, required this.todo}) : super(key: key);
  final List<Todo> todo;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Todos'),
        
        ),
        body: ListView.builder(itemCount: todo.length,itemBuilder:(context,index){
          return ListTile(
            title: Text(todo[index].title),
            onTap: (){
              Navigator.push(context, 
              MaterialPageRoute(builder: (context) => DetailScreen(todo: todo[index]),
              // settings: RouteSettings(
              //   arguments: todo[index])
              ),
              );
            },
          );
        }),
    );
  }
}

class DetailScreen extends StatelessWidget {
  const DetailScreen({Key? key,required this.todo}) : super(key: key);
  final Todo todo;
  @override
  Widget build(BuildContext context) {
    //final todo = ModalRoute.of(context)!.settings.arguments as Todo;
    return Scaffold(
      appBar: AppBar(title:Text(todo.title)
      ),
      body: Padding(
        padding:  const EdgeInsets.all(16.0),
        child: Text(todo.description),
      ),
    );
  }
}