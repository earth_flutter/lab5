import 'dart:js';

import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    title: 'Named Routes Demo',
    initialRoute: '/',
    routes: {
      '/':(context) => const FirstScreen()
      ,
      '/Second':(context) => const SeconScreen(),
      '/Third':(context) => const ThirdScreen()
      
    },
  ));
}

class FirstScreen extends StatelessWidget {
  const FirstScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('First Screen'),
      ),
      body: Center(

        child: Column(children: [
          SizedBox(height: 8.0),
          ElevatedButton(
          child: const Text('Lunch Second Screen'),
          onPressed: (){
            Navigator.pushNamed(context, '/Second');
          },
          ),
          SizedBox(height: 8.0),
          ElevatedButton(
          child: const Text('Lunch Third Screen'),
          onPressed: (){
            Navigator.pushNamed(context, '/Third');
          },
          ),
          ],)
          ),
    );
  }
}

class SeconScreen extends StatelessWidget {
  const SeconScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Secon Screen'),
      ),
      body: Center(child: ElevatedButton(
        child: const Text('Go back'),
        onPressed: (){
          Navigator.pop(context);
        }
        ,),),
    );
  }
}

class ThirdScreen extends StatelessWidget {
  const ThirdScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Third Screen'),
      ),
      body: Center(
        child: Column(children: [
          SizedBox(height: 8,),
          ElevatedButton(
        child: const Text('Go to Second Screen'),
        onPressed: (){
          Navigator.pushNamed(context, '/Second');
        }
        ,),
        SizedBox(height: 8,),
        ElevatedButton(
        child: const Text('Go back'),
        onPressed: (){
          Navigator.pop(context);
        }
        ,)
        ],) ,),
    );
  }
}