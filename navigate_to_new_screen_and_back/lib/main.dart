import 'package:flutter/material.dart';

void main(){
  runApp(MaterialApp(
    home: FirstRoute(),
  ));
}


class FirstRoute extends StatelessWidget {
  const FirstRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('First Route'),),
      body: Center(
        child : Column(
          children: [
          ElevatedButton(
          child: Text('Open route 2'),
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context){
              return SeconRoute();
            }));
          },
          ),
          SizedBox(
            height: 10.0,
          ),
          ElevatedButton(
          child: Text('Open route 3'),
          onPressed: (){
            Navigator.push(context, MaterialPageRoute(builder: (context){
              return ThirdRoute();
            }));
          },
          )
        ],)
        ),
    );
  }
}

class SeconRoute extends StatelessWidget {
  const SeconRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Secon Route'),),
      body: Center(
        child: ElevatedButton(
          child: Text('Go back '),
          onPressed: (){
            Navigator.pop(context);
          },
          ) ,),
    );
  }
}

class ThirdRoute extends StatelessWidget {
  const ThirdRoute({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('Third Route'),),
      body: Center(
        child: ElevatedButton(
          child: Text('Go back '),
          onPressed: (){
            Navigator.pop(context);
          },
          ) ,),
    );
  }
}